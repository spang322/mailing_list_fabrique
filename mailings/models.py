from mailings.db import get_db

class Client:
    def __init__(self, name, phone_number, operator_code, tag, time_zone):
        self.name = name
        self.phone_number = phone_number
        self.operator_code = operator_code
        self.tag = tag
        self.time_zone = time_zone

    @staticmethod
    def create_table():
        db = get_db()
        db.execute('CREATE TABLE clients (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, phone_number TEXT NOT NULL UNIQUE, operator_code TEXT NOT NULL, tag TEXT NOT NULL, time_zone TEXT NOT NULL)')
        db.commit()

    def save(self):
        db = get_db()
        db.execute('INSERT INTO clients (name, phone_number, operator_code, tag, time_zone) VALUES (?, ?, ?, ?, ?)', (self.name, self.phone_number, self.operator_code, self.tag, self.time_zone))
        db.commit()

    @staticmethod
    def get_all():
        db = get_db()
        clients = db.execute('SELECT * FROM clients').fetchall()
        return [Client(*client) for client in clients]

class MailingList:
    def __init__(self, name, message_text):
        self.name = name
        self.message_text = message_text

    @staticmethod
    def create_table():
        db = get_db()
        db.execute('CREATE TABLE mailing_lists (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, message_text TEXT NOT NULL)')
        db.commit()

    def save(self):
        db = get_db()
        db.execute('INSERT INTO mailing_lists (name, message_text) VALUES (?, ?)', (self.name, self.message_text))
        db.commit()

    @staticmethod
    def get_all():
        db = get_db()
        mailing_lists = db.execute('SELECT * FROM mailing_lists').fetchall()
        return [MailingList(*mailing_list) for mailing_list in mailing_lists]

class Message:
    def __init__(self, mailing_list_id, client_id, date_sent):
        self.mailing_list_id = mailing_list_id
        self.client_id = client_id
        self.date_sent = date_sent

    @staticmethod
    def create_table():
        db = get_db()
        db.execute('CREATE TABLE messages (id INTEGER PRIMARY KEY AUTOINCREMENT, mailing_list_id INTEGER NOT NULL, client_id INTEGER NOT NULL, date_sent TEXT NOT NULL, FOREIGN KEY(mailing_list_id) REFERENCES mailing_lists(id), FOREIGN KEY(client_id) REFERENCES clients(id))')
        db.commit()

    def save(self):
        db = get_db()
        db.execute('INSERT INTO messages (mailing_list_id, client_id, date_sent) VALUES (?, ?, ?)', (self.mailing_list_id, self.client_id, self.date_sent))
        db.commit()

    @staticmethod
    def get_all():
        db = get_db()
        messages = db.execute('SELECT * FROM messages').fetchall()
        return [Message(*message) for message in messages]