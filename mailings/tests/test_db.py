import unittest
from mailings import db
from mailings.models import Client, MailingList, Message

class TestDatabase(unittest.TestCase):
    def setUp(self):
        self.client = Client(name='Test Client', phone_number='1234567890', operator_code='123', tag='test', time_zone='UTC')
        self.mailing_list = MailingList(name='Test Mailing List', message_text='Test Message')
        self.message = Message(message_id='1234567890', creation_time='2021-01-01 00:00:00', sending_status='Sent', mailing_id=self.mailing_list.id, client_id=self.client.id)

        db.session.add(self.client)
        db.session.add(self.mailing_list)
        db.session.add(self.message)
        db.session.commit()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_client_added_to_database(self):
        test_client = Client.query.filter_by(name='Test Client').first()
        self.assertIsNotNone(test_client)
        self.assertEqual(test_client.phone_number, '1234567890')

    def test_mailing_list_added_to_database(self):
        test_mailing_list = MailingList.query.filter_by(name='Test Mailing List').first()
        self.assertIsNotNone(test_mailing_list)
        self.assertEqual(test_mailing_list.message_text, 'Test Message')

    def test_message_added_to_database(self):
        test_message = Message.query.filter_by(message_id='1234567890').first()
        self.assertIsNotNone(test_message)
        self.assertEqual(test_message.sending_status, 'Sent')