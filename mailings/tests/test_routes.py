import unittest
from flask import url_for
from mailings import app, db
from mailings.models import Client, MailingList
from mailings.routes import add_client, add_mailing_list, update_mailing_list, delete_mailing_list

class TestRoutes(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        self.app = app.test_client()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_add_client_route(self):
        response = self.app.post('/add_client', data=dict(name='Test Client', phone_number='1234567890', operator_code='123', tag='test', time_zone='UTC'), follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        added_client = Client.query.filter_by(name='Test Client').first()
        self.assertIsNotNone(added_client)
        self.assertEqual(added_client.phone_number, '1234567890')

    def test_add_mailing_list_route(self):
        response = self.app.post('/add_mailing_list', data=dict(name='Test Mailing List', message_text='Test Message'), follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        added_mailing_list = MailingList.query.filter_by(name='Test Mailing List').first()
        self.assertIsNotNone(added_mailing_list)
        self.assertEqual(added_mailing_list.message_text, 'Test Message')

    def test_update_mailing_list_route(self):
        mailing_list = MailingList(name='Test Mailing List', message_text='Test Message')
        db.session.add(mailing_list)