import uuid
from datetime import datetime
from flask import render_template, request, redirect, url_for, flash
from mailings import app, db
from mailings.models import Client, MailingList, Message
import requests
from mailings.config import PROBE_API_KEY

@app.route('/')
def index():
    mailing_lists = MailingList.query.all()
    clients = Client.query.all()
    return render_template('index.html', mailing_lists=mailing_lists, clients=clients)

@app.route('/add_client', methods=['GET', 'POST'])
def add_client():
    if request.method == 'POST':
        name = request.form['name']
        phone_number = request.form['phone_number']
        operator_code = request.form['operator_code']
        tag = request.form['tag']
        time_zone = request.form['time_zone']

        new_client = Client(name=name, phone_number=phone_number, operator_code=operator_code, tag=tag, time_zone=time_zone)
        db.session.add(new_client)
        db.session.commit()

        flash('New client added successfully')
        return redirect(url_for('index'))

    return render_template('add_client.html')

@app.route('/add_mailing_list', methods=['GET', 'POST'])
def add_mailing_list():
    if request.method == 'POST':
        name = request.form['name']
        message_text = request.form['message_text']

        new_mailing_list = MailingList(name=name, message_text=message_text)
        db.session.add(new_mailing_list)
        db.session.commit()

        flash('New mailing list added successfully')
        return redirect(url_for('index'))

    return render_template('add_mailing_list.html')

@app.route('/update_mailing_list/<int:id>', methods=['GET', 'POST'])
def update_mailing_list(id):
    mailing_list = MailingList.query.get_or_404(id)

    if request.method == 'POST':
        mailing_list.name = request.form['name']
        mailing_list.message_text = request.form['message_text']

        db.session.commit()

        flash('Mailing list updated successfully')
        return redirect(url_for('index'))

    return render_template('update_mailing_list.html', mailing_list=mailing_list)

@app.route('/delete_mailing_list/<int:id>', methods=['GET', 'POST'])
def delete_mailing_list(id):
    mailing_list = MailingList.query.get_or_404(id)

    if request.method == 'POST':
        db.session.delete(mailing_list)
        db.session.commit()

        flash('Mailing list deleted successfully')
        return redirect(url_for('index'))

    return render_template('delete_mailing_list.html', mailing_list=mailing_list)

@app.route('/send_mailing_list/<int:id>', methods=['GET', 'POST'])
def send_mailing_list(id):
    mailing_list = MailingList.query.get_or_404(id)

    if request.method == 'POST':
        clients = Client.query.all()
        mailing_id = mailing_list.id

        for client in clients:
            message_id = uuid.uuid4()
            creation_time = datetime.utcnow()
            sending_status = "Pending"

            new_message = Message(message_id=message_id, creation_time=creation_time, sending_status=sending_status, mailing_id=mailing_id, client_id=client.id)
            db.session.add(new_message)
            db.session.commit()

            # Send message to client using Probe API
            url = "https://probe.fbrq.cloud/send"
            payload = {
                "recipient": client.phone_number,
                "message_id": str(message_id),
                "message_text": mailing_list.message_text
            }
            headers = {
                "Content-Type": "application/json",
                "Authorization": f"Bearer {PROBE_API_KEY}"
            }

            response = requests.post(url, json=payload, headers=headers)

            if response.status_code == 200:
                new_message.sending_status = "Sent"
            else:
                new_message.sending_status = "Failed"

            db.session.commit()

        flash('Mailing list sent successfully')
        return redirect(url_for('index'))

    return render_template('send_mailing_list.html', mailing_list=mailing_list)
