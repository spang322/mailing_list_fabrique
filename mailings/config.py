import os

# Database settings
basedir = os.path.abspath(os.path.dirname(__file__))
DATABASE = os.path.join(basedir, 'app.db')

# API key for Probe messaging service
PROBE_API_KEY = 'YOUR_API_KEY'